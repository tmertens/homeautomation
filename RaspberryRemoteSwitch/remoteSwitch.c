#include <time.h>
#include <stdint.h>
#include <string.h>
//#include <bcm2835.h>

// Access from ARM Running Linux

#define BCM2708_PERI_BASE        0x20000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

//define burst length in ns
#define SHORT_BURST 370000
#define LONG_BURST 3*SHORT_BURST

int  mem_fd;
void *gpio_map;

// I/O access
volatile unsigned *gpio;


// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or SET_GPIO_ALT(x,y)
#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))

#define GPIO_SET *(gpio+7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10) // clears bits which are 1 ignores bits which are 0

void setup_io();

//
// Set up a memory regions to access GPIO
//
void setup_io()
{
   /* open /dev/mem */
   if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
      printf("can't open /dev/mem \n");
      exit(-1);
   }

   /* mmap GPIO */
   gpio_map = mmap(
      NULL,             //Any adddress in our space will do
      BLOCK_SIZE,       //Map length
      PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
      MAP_SHARED,       //Shared with other processes
      mem_fd,           //File to map
      GPIO_BASE         //Offset to GPIO peripheral
   );

   close(mem_fd); //No need to keep mem_fd open after mmap

   if (gpio_map == MAP_FAILED) {
      printf("mmap error %d\n", (int)gpio_map);//errno also set!
      exit(-1);
   }

   // Always use volatile pointer!
   gpio = (volatile unsigned *)gpio_map;
   printf("SETUP FINISHED\n");
} // setup_io


/* this will register an contructor that calls the setup_io() when the app starts */
static void con() __attribute__((constructor));

void con() {
    setup_io();
}



/* use this to send power to a pin */
void set_pin(int pin) {
	GPIO_SET = 1<<pin;
}

/* use this to set no power to a pin */
void clr_pin(int pin) {
	GPIO_CLR = 1<<pin;
}

/**
* sends a one (decode scheme: on - wait 370µs - off - wait 1110µs - on - wait 370µs - off - wait 1110µs) via PIN 17
*/
void sendOne()
{
    //declare and initialize the structs for nanosleep
    struct timespec tm1, tm2;
    tm1.tv_sec = 0;
    tm1.tv_nsec = SHORT_BURST;

    //set PIN 17 - on
    set_pin(17);
    //wait 370µs
    nanosleep(&tm1, &tm2);
    //clear PIN 17 - off
    clr_pin(17);
    //wait 1110µs
    nanosleep(&tm1, &tm2);
    nanosleep(&tm1, &tm2);
    nanosleep(&tm1, &tm2);
    //set PIN 17 - on
    set_pin(17);
    //wait 370µs
    nanosleep(&tm1, &tm2);
    //clear PIN 17 - off
    clr_pin(17);
    //wait 1110µs
    nanosleep(&tm1, &tm2);
    nanosleep(&tm1, &tm2);
    nanosleep(&tm1, &tm2);
}

/**
* sends a zero (decode scheme: on - wait 370µs - off - wait 1110µs - on - wait 370µs - off - wait 1110µs) via PIN 17
*/
void sendZero()
{
    //declare and initialize the structs for nanosleep
    struct timespec tm1, tm2;
    tm1.tv_sec = 0;
    tm1.tv_nsec = SHORT_BURST;
    
    //set PIN 17 - on
    set_pin(17);
    //wait 370µs
    nanosleep(&tm1, &tm2);
    //clear PIN 17 - off
    clr_pin(17);
    //wait 1110µs
    nanosleep(&tm1, &tm2);
    nanosleep(&tm1, &tm2);
    nanosleep(&tm1, &tm2);
    //set PIN 17 - on
    set_pin(17);
    //wait 1110µs
    nanosleep(&tm1, &tm2);
    nanosleep(&tm1, &tm2);
    nanosleep(&tm1, &tm2);
    //clear PIN 17 - off
    clr_pin(17);
    //wait 370µs
    nanosleep(&tm1, &tm2);
}


int main(int argv, char** argc)
{
    //declare and initialize the structs for nanosleep
    struct timespec tm1, tm2;
    tm1.tv_sec = 0;
    tm1.tv_nsec =  SHORT_BURST;
    
    //check if all necessary parameters are set
    if(argv != 3)
    {
        //if not: Print error message and exit
        fprintf(stderr, "Usage of remoteSwitch: PlugCode On/Off\n");
        return EXIT_FAILURE;
    }

    //allocate memory for the message which should be send
    char *message = calloc(sizeof(char), 12);
    //check if allocation succeeded
    if(message == NULL)
    {
        //if not: Print error message and exit
        perror("Calloc failed");
        return EXIT_FAILURE;
    }
    
    //copy plugcode from parameters to send buffer
    strncpy(message, argc[1], 10);

    //determine if ON or OFF code should be sent
    if(strncmp(argc[2], "On", 2) == 0)
    {
        printf("0n\n");
        //if ON: Append the code for ON to the message
        strncpy(&message[10], "10", 2);
    }
    else
    {
        printf("Off\n");
        //if OFF: Append the code for OFF to the message
        strncpy(&message[10], "01", 2);
    }

    //set PIN17 as output
    INP_GPIO(17); // must use INP_GPIO before we can use OUT_GPIO
    OUT_GPIO(17);

    int j;
    //send signal 5 times to ensure the execution
    for(j = 0; j < 5; j++)
    {
       int i;
       //iterate over every bit of the message
       for(i = 0; i < 12; i++)
       {
           if(message[i] == '0')
           {
              //if bit is not set: Send a zero
              sendZero();
           }
           else
           {
              //if bit is set: Send a one
              sendOne();
           }
       }
      //send synchronization: on - wait 370µs - off - wait 13320µs
      set_pin(17);
      nanosleep(&tm1, &tm2);
      clr_pin(17);

      tm1.tv_nsec = 36*SHORT_BURST;
      nanosleep(&tm1, &tm2);
   }

   return EXIT_SUCCESS;
}

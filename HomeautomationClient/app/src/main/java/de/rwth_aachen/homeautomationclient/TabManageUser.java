package de.rwth_aachen.homeautomationclient;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class TabManageUser extends AbstractTab {
    private View view;

    @Override
    //Creates view and manages onClick events
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tab_manageuser, container, false);

        //Button user add
        view.findViewById(R.id.btn_useradd).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String name = ((EditText) view.findViewById(R.id.adduser_name)).getText().toString();
                String password = ((EditText) view.findViewById(R.id.adduser_pwd)).getText().toString();
                String tmp = Messages.createMessageType203(name,password);

                MainActivity.sendMessage(tmp);

                ((EditText) view.findViewById(R.id.adduser_name)).setText("");
                ((EditText) view.findViewById(R.id.adduser_pwd)).setText("");
            }
        });

        //Button change password
        view.findViewById(R.id.btn_changepwd).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String name = ((EditText) view.findViewById(R.id.edituser_name)).getText().toString();
                String passwordOld = ((EditText) view.findViewById(R.id.edituser_old)).getText().toString();
                String passwordNew= ((EditText) view.findViewById(R.id.edituser_new)).getText().toString();
                String tmp = Messages.createMessageType205(name, passwordOld, passwordNew);

                MainActivity.sendMessage(tmp);
                ((EditText) view.findViewById(R.id.edituser_name)).setText("");
                ((EditText) view.findViewById(R.id.edituser_new)).setText("");
                ((EditText) view.findViewById(R.id.edituser_old)).setText("");
            }
        });

        //Button delete user
        view.findViewById(R.id.btn_deleteuser).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String name = ((EditText) view.findViewById(R.id.edituser_name)).getText().toString();
                String passwordOld = ((EditText) view.findViewById(R.id.edituser_old)).getText().toString();
                String tmp = Messages.createMessageType204(name,passwordOld);

                MainActivity.sendMessage(tmp);
                ((EditText) view.findViewById(R.id.edituser_name)).setText("");
                ((EditText) view.findViewById(R.id.edituser_old)).setText("");
            }
        });
        return view;
    }

    @Override
    public void updateView(Context context, String[] plugs, String[] status, String[] id) {

    }

}

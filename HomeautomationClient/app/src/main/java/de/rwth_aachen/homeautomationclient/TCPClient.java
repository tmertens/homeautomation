package de.rwth_aachen.homeautomationclient;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class TCPClient {

    private String serverMessage;
    public final String host;
    public final int port;
    private OnMessageReceived mMessageListener = null;
    private boolean mRun = false;
    Context context;

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    private boolean connected = false;

    private BufferedWriter out;

    /**
     *  Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TCPClient(String host, int port, Context context, OnMessageReceived listener) {
        this.host = host;
        this.port = port;
        this.context = context;
        mMessageListener = listener;
    }

    /**
     * Sends the message entered by client to the server
     * @param message String
     */
    public void sendMessage(String message) {
        if (out != null) {
            try {
                out.write(message +"\n");
                out.flush();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * stops the client
     */
    public void stopClient(){
        sendMessage("");
        mRun = false;
    }

    /**
     *
     */
    public void run() {
        mRun = true;
        try {
            //Get address of host
            InetAddress serverAddr = InetAddress.getByName(host);
            // open TLS connection
            KeyStore store = KeyStore.getInstance("BKS");
            InputStream is = context.getResources().openRawResource(R.raw.client);
            store.load(is, "client".toCharArray());
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(store);
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(store, "client".toCharArray());
            // init SSL context
            SSLContext context = SSLContext.getInstance("TLSv1.1");
            context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new java.security.SecureRandom());
            // open client socket
            SSLSocketFactory factory = context.getSocketFactory();
            SSLSocket socket = (SSLSocket)factory.createSocket(serverAddr, port);
            // start handshake
            socket.setUseClientMode(true);
            socket.startHandshake();
            setConnected(socket.isConnected());
            try {
                //send the message to the server
                out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                //receive the message which the server sends back
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                //in this while the client listens for the messages sent by the server
                while (mRun) {
                    serverMessage = in.readLine();
                    if (serverMessage != null && mMessageListener != null) {
                        //call the method messageReceived
                        mMessageListener.messageReceived(serverMessage);
                    }
                    serverMessage = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //Declare the interface. The method messageReceived(String message) will is implemented in the MainActivity
    //class at on asynckTask doInBackground
    public interface OnMessageReceived {
        public void messageReceived(String message);
    }
}
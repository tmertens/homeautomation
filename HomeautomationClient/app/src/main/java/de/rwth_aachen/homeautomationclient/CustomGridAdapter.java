package de.rwth_aachen.homeautomationclient;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class CustomGridAdapter extends BaseAdapter {

    private Context context;
    private String[] plugs = null;
    private String[] status = null;
    private String[] id = null;

    //Constructor to initialize values
    public CustomGridAdapter(Context context, String[ ] plugs, String[] status, String[] id) {
            this.context = context;
            this.plugs = plugs;
            this.status = status;
            this.id = id;

    }

    @Override
    public int getCount() {
        // Number of times getView method call depends upon gridValues.length
        return plugs.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    // Number of times getView method call depends upon gridValues.length
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // LayoutInflator to call external grid_item.xml file

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            // get layout from grid_item.xml
            gridView = inflater.inflate( R.layout.grid_item , null);
        } else {
            gridView = convertView;
        }
            // set plugname into textview
            TextView name_text = (TextView) gridView.findViewById(R.id.grid_item_name);
            name_text.setText(plugs[position]);

            // set button on status
            Button button = (Button) gridView.findViewById(R.id.grid_item_image);
            if(status[position].equals("0")){
                button.setText("OFF");
                button.setBackgroundColor(Color.RED);
            } else if(status[position].equals("1")){
                button.setText("ON");
                button.setBackgroundColor(Color.GREEN);
            }

            //set button onClick
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // invert current state
                    int new_status= 1 - Integer.parseInt(status[position]);
                    String message = Messages.createMessageType1(Integer.parseInt(id[position]),new_status);
                    MainActivity.sendMessage(message);
                    MainActivity.setLastAction(4);
                }
            });
        return gridView;
    }
}
package de.rwth_aachen.homeautomationclient;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.HashMap;

class TabsPagerAdapter extends FragmentPagerAdapter {

    // Number of tabs
    private static final int NUM_TABS=3;
    // Tab titles
    private final String[] tabTitles = { "Switch Plugs", "Manage Plugs", "Manage User"};

    private final HashMap<Integer, AbstractTab> tabs;

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
        tabs = new HashMap<>();
    }

    @Override
    public AbstractTab getItem(int index) {

        AbstractTab tab = null;
        switch(index){
            case 0: tab = new TabSwitching(); break;
            case 1: tab = new TabManagePlug(); break;
            case 2: tab = new TabManageUser(); break;
        }

        // Pass index to tab
        Bundle args = new Bundle();
        args.putInt("index", index);
        if (tab != null) {
            tab.setArguments(args);
        }

        // Add new tab to hashset
        tabs.put(index, tab);

        return tab;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return NUM_TABS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    public AbstractTab getTab(int position) {
        if(position<0 || position>=NUM_TABS){
            return null;
        }
        return tabs.get(position);
    }

}

package de.rwth_aachen.homeautomationclient;

public class Messages {

    /**
     * Type 1 - Switch plug
     * @param id plug id
     * @param state new state
     * @return message as String
     */
    public static String createMessageType1(int id, int state){
        String message = "Type: 1;";
        message += "Id: "+id+";";
        message += "State: "+state+";";
        message += ";";
        return message;
    }

    /**
     * Type 3 - Login message
     * @param username username
     * @param pw password
     * @return message as String
     */
    public static String createMessageType3(String username, String pw){
        String message = "Type: 3;";
        message += "User: "+username+";";
        message += "Password: "+pw+";";
        message += ";";
        return message;
    }

    /**
     * Type 4 - Logout message
     * @return message as String
     */
    public static String createMessageType4(){
        String message = "Type: 4;";
        message += ";";
        return message;
    }

     /**
     * Type 201 - Add plug
     * @return message as String
     */
    public static String createMessageType201(int id, String name, int code){
        String message = "Type: 201;";
        message += "Id: "+id+";";
        message += "Name: "+name+";";
        message += "Code: "+code+";";
        message += ";";
        return message;
    }

    /**
     * Type 202 - Remove plug
     * @return message as String
     */
    public static String createMessageType202(int id){
        String message = "Type: 202;";
        message += "Id: "+id+";";
        message += ";";
        return message;
    }

    /**
     * Type 203 - Add user
     * @return message as String
     */
    public static String createMessageType203(String name, String pw){
        String message = "Type: 203;";
        message += "User: "+name+";";
        message += "Password: "+pw+";";
        message += ";";
        return message;
    }

    /**
     * Type 204 - Remove user
     * @return message as String
     */
    public static String createMessageType204(String name, String pw){
        String message = "Type: 204;";
        message += "User: "+name+";";
        message += "Password: "+pw+";";
        message += ";";
        return message;
    }

    /**
     * Type 205 - Change user password
     * @return message as String
     */
    public static String createMessageType205(String name, String oldPw, String newPw){
        String message = "Type: 205;";
        message += "User: "+name+";";
        message += "OldPassword: "+oldPw+";";
        message += "NewPassword: "+newPw+";";
        message += ";";
        return message;
    }
}

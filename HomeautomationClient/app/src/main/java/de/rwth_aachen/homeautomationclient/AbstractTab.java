package de.rwth_aachen.homeautomationclient;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class AbstractTab extends Fragment {

    // Define default tab layout
    public abstract View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    // Display new data on tab
    public abstract void updateView(Context context, String[] plugs, String[] status, String[] id);

}

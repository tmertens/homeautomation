package de.rwth_aachen.homeautomationclient;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class TabManagePlug extends AbstractTab {

    private View view;
    private String[] id;

    @Override
    //Creates view and manages onClick events
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.tab_addplug, container, false);

        //Button plug add
        view.findViewById(R.id.btnaddplug).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String code = ((EditText) view.findViewById(R.id.plugaddcode)).getText().toString();
                String id = ((EditText) view.findViewById(R.id.plugaddid)).getText().toString();
                String name = ((EditText) view.findViewById(R.id.plugaddname)).getText().toString();

                String message = Messages.createMessageType201(Integer.parseInt(id),name,Integer.parseInt(code));
                MainActivity.setLastAction(2);
                MainActivity.sendMessage(message);

                ((EditText) view.findViewById(R.id.plugaddcode)).setText("");
                ((EditText) view.findViewById(R.id.plugaddid)).setText("");
                ((EditText) view.findViewById(R.id.plugaddname)).setText("");
            }
        });

        //Button plug remove
        view.findViewById(R.id.btnremoveplug).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Spinner dropdown = (Spinner) view.findViewById(R.id.spinner1);
                int plugId = Integer.parseInt(id[dropdown.getSelectedItemPosition()]);

                String tmp = Messages.createMessageType202((plugId));
                MainActivity.setLastAction(3);
                MainActivity.sendMessage(tmp);
            }
        });
        return view;
    }

    @Override
    //Update data in spinner
    public void updateView(Context context, String[] plugs, String[] status, String[] id) {
        this.id = id;
        Spinner dropdown = (Spinner) view.findViewById(R.id.spinner1);
        ArrayAdapter<String> LTRadapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_item, plugs);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        dropdown.setAdapter(LTRadapter);
    }

}

package de.rwth_aachen.homeautomationclient;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

    public static TCPClient tcpClient = null;
    String[ ] plugs = new String[] { };
    String[ ] status = new String[] { };
    String[ ] id = new String[] { };

    private static int lastAction=0;

    Button btnLogin,btnConnect,btnAddPlug;

    EditText editHost,editPort,editPlugaddId,editPlugaddName,editPlugaddCode;
    private ViewPager viewPager;
    private TabsPagerAdapter tabsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnConnect = (Button)findViewById(R.id.connect);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnAddPlug = (Button)findViewById(R.id.btnaddplug);

        editHost = (EditText) findViewById(R.id.host);
        editPort = (EditText) findViewById(R.id.port);

        editPlugaddId = (EditText) findViewById(R.id.plugaddid);
        editPlugaddName = (EditText) findViewById(R.id.plugaddname);
        editPlugaddCode = (EditText) findViewById(R.id.plugaddcode);

        viewPager = (ViewPager) findViewById(R.id.pager);
        tabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabsPagerAdapter);

        viewPager.setOffscreenPageLimit(3);
    }

    public static void sendMessage(final String message)
    {
        new Thread()
        {
            public void run()
            {
                tcpClient.sendMessage(message);
            }
        }.start();
    }

    public static void stopClient()
    {
        new Thread()
        {
            public void run()
            {
                tcpClient.stopClient();
            }
        }.start();
    }

    @Override
    protected void onPause() {
        if(tcpClient!=null)
            stopClient();
        super.onPause();
    }

    @Override
    protected void onStop() {
        if(tcpClient!=null)
            stopClient();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if(tcpClient!=null)
            stopClient();
        super.onDestroy();
    }

    /**
     * returns lastAction
     * @return lastAction
     */
    public int getLastAction() {
        return lastAction;
    }

    /**
     * sets lastAction
     * @param _lastAction int
     */
    public static void setLastAction(int _lastAction){
        lastAction = _lastAction;
    }

    /**
     * Calls the updateView functions of the tabs to update tabs
     * @param plugs contains plug name
     * @param status contains plug status
     * @param id contains plug id
     */
    public void updateView(String[] plugs, String[] status, String[] id){
        AbstractTab tab = tabsPagerAdapter.getTab(0);
        tab.updateView(this, plugs, status, id);

        tab = tabsPagerAdapter.getTab(1);
        tab.updateView(this, plugs, status, id);
    }

    /**
     * Manages onClick Events, updates Edits and Buttons, calls functions
     * @param v View
     */
    public void onClick(View v) {
        String username = ((EditText) findViewById(R.id.user)).getText().toString();
        String password = ((EditText) findViewById(R.id.pwd)).getText().toString();
        switch (v.getId()) {
            case  R.id.connect: {
                if(btnConnect.getText().equals("Connect")) {
                    //Connect
                    connectTask task = new connectTask();
                    task.setContext(this.getApplicationContext());
                    task.execute(editHost.getText().toString(), editPort.getText().toString());
                }else {
                    //Disconnect
                    if (btnConnect.getText().equals("Disconnect")) {
                        editHost.setEnabled(true);
                        editPort.setEnabled(true);
                        if (btnLogin.getText().equals("Logout")) {
                            //Logout
                            String tmp = Messages.createMessageType4();
                            sendMessage(tmp);
                            btnLogin.setText("Login");
                        }
                        stopClient();
                        viewPager.setVisibility(View.INVISIBLE);
                        btnConnect.setText("Connect");
                        btnLogin.setVisibility(View.INVISIBLE);
                    }
                }

                break;
            }
            case R.id.btnLogin:{
                if ((tcpClient != null)&&(tcpClient.isConnected())) {
                    if(btnLogin.getText().equals("Login")) {
                        //Login
                        String tmp = Messages.createMessageType3(username, password);
                        sendMessage(tmp);
                        setLastAction(1);
                    }else if(btnLogin.getText().equals("Logout")){
                        //Logout
                        String tmp = Messages.createMessageType4();
                        sendMessage(tmp);
                        viewPager.setVisibility(View.INVISIBLE);
                        findViewById(R.id.user).setEnabled(true);
                        findViewById(R.id.pwd).setEnabled(true);
                        btnLogin.setText("Login");
                    }
                }else{
                    System.out.println("error");
                }
                break;
            }
        }
    }

     @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        // on tab selected show respected fragment view
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    public class connectTask extends AsyncTask<String,String,TCPClient> {

        private Context context;

        public void setContext(Context context){
            this.context = context;
        }

        @Override
        protected TCPClient doInBackground(String... message) {
            //we create a TCPClient object and
            tcpClient= new TCPClient(message[0],Integer.parseInt(message[1]), getApplicationContext(),new TCPClient.OnMessageReceived() {
                @Override
                //here the messageReceived method is implemented
                public void messageReceived(String message) {
                    //Parsing header
                    HashMap<String, String> header = Parser.parseHeader(message);

                    if(header.get("Type").equals("2"))
                    {
                        //Plug list
                        List<String> plugs_list = new ArrayList<>();
                        List<String> id_list = new ArrayList<>();
                        List<String> status_list = new ArrayList<>();
                        int elements = Integer.parseInt(header.get("Length"));
                        for(int i=0; i<elements ;i++){
                            String info = header.get("Plug"+(i+1));
                            String[] split = info.split(",");
                            if(split.length==3){
                                id_list.add(split[0]);
                                plugs_list.add(split[1]);
                                status_list.add(split[2]);
                            }
                        }
                        plugs = plugs_list.toArray(new String[plugs_list.size()]);
                        status = status_list.toArray(new String[status_list.size()]);
                        id = id_list.toArray(new String[id_list.size()]);
                        //Update Tabs and create notification
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(R.id.user).setEnabled(false);
                                findViewById(R.id.pwd).setEnabled(false);
                                viewPager.setVisibility(View.VISIBLE);
                                switch (getLastAction()){
                                    case 0:
                                        Toast.makeText(context, "Received new plug list", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 1:
                                        Toast.makeText(context, "Login successful", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 2:
                                        Toast.makeText(context, "Plug added. Received new plug list.", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 3:
                                        Toast.makeText(context, "Plug removed. Received new plug list.", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 4:
                                        Toast.makeText(context, "Received new plug list after plug switched", Toast.LENGTH_SHORT).show();
                                        break;
                                }
                                setLastAction(0);
                                btnLogin.setText("Logout");
                                updateView(plugs,status,id);
                            }
                        });
                    }
                    else if(header.get("Type").equals("100"))
                    {
                        //Error Message
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, "There occurred an error.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    else if(header.get("Type").equals("0"))
                    {
                        //Connection accepted
                        tcpClient.setConnected(true);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnLogin.setVisibility(View.VISIBLE);
                                btnConnect.setText("Disconnect");
                                editHost.setEnabled(false);
                                editPort.setEnabled(false);
                            }
                        });
                    }
                }
            });
            tcpClient.run();

            return null;
        }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

}

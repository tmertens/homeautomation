package de.rwth_aachen.homeautomationclient;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

public class TabSwitching extends AbstractTab {

    @Override
    //Creates view
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_switch, container, false);
    }

    @Override
    //Update data of plugs
    public void updateView(Context context, String[] plugs, String[] status, String[] id) {
        if(getView()!=null) {
            GridView gridView = (GridView) getView().findViewById(R.id.gridView1);
            if (gridView != null) {
                gridView.setVisibility(View.VISIBLE);
                gridView.setAdapter(new CustomGridAdapter(context, plugs, status, id));
            } else {
                System.out.println("GridView not found");
            }
        }
    }
}

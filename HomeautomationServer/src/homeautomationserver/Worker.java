/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeautomationserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

/**
 * Manages client connection and handles incoming messages
 * @author admin
 */
public class Worker extends Thread{
 
    private final MultiThreadedServer serverInstance;
    private Socket clientSocket = null;
    // incoming/outgoing message buffer
    private BufferedReader bufferIn=null;
    private PrintWriter bufferOut=null;
    private boolean loggedIn;
    
    /**
     * Default construtor
     * @param serverInstance corresponding server instance
     * @param clientSocket client socket
     */
    public Worker(MultiThreadedServer serverInstance,Socket clientSocket){
        this.serverInstance = serverInstance;
        this.clientSocket = clientSocket;
        this.loggedIn = false;
    }
    
    @Override
    public void run(){     
        String message;
        try{
            // init buffers
            bufferIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            bufferOut = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            
            sendMessage(Messages.createMessageType0());
            
            while((!this.isInterrupted())){
                // wait for new messages from client
                message = bufferIn.readLine();
                try{
                    handleMessage(message);       
                }catch(RuntimeException e){
                    return;
                }
            }
        }catch(IOException e){
            throw new RuntimeException("Error receiving message", e);
        }finally{
            System.out.println("Client connection closed.");
            try{
                clientSocket.close();
                if(bufferIn != null){
                    bufferIn.close();
                }
                if(bufferOut != null){
                    bufferOut.flush();
                    bufferOut.close();
                }
            }catch(IOException e){
                throw new RuntimeException("Error closing buffers", e);
            }        
        }
        
        // Terminate thread
        serverInstance.removeWorker(this);
    }
    
    /**
     * Sends message to client
     * @param message message to be send
     */
    public void sendMessage(String message){
        if(bufferOut!=null && !bufferOut.checkError()){
            bufferOut.println(message);
            bufferOut.flush();
        }
    }
    
    /**
     * Handles incoming messages
     * @param message incoming message
     */
    private void handleMessage(String message){
        System.out.println("Received message: "+message);
        
        // first parse received message
        HashMap<String, String> header = Parser.parseHeader(message);       
        String response;
        if(header != null){
            // check for type field
            if(!header.containsKey("Type")){
                throw new RuntimeException("Error missing message type");
            }
        
            // not logged in yet
            if(!loggedIn && Integer.parseInt(header.get("Type"))!=3){
                sendMessage(Messages.createMessageType100());
                return;
            }
        
        // distinguish message types
            switch(Integer.parseInt(header.get("Type"))){
                // handle all message types    
                case 1: 
                    int id = Integer.parseInt(header.get("Id"));
                    int newState = Integer.parseInt(header.get("State"));
                    int code = Configuration.getPlug(id).getCode();

		try
		{
                    // connect to C interface and send signal
                    if(newState == 1){
                        new ProcessBuilder("/usr/bin/sudo", "/home/pi/assignment4/RaspberryRemoteSwitch/remoteSwitch", String.format("%010d", code), "On").start();        
                    }else{
			new ProcessBuilder("/usr/bin/sudo", "/home/pi/assignment4/RaspberryRemoteSwitch/remoteSwitch", String.format("%010d", code), "Off").start();
                    }
                }
		catch(IOException ex)
		{
			ex.printStackTrace();
			throw new RuntimeException("Error calling c interface", ex);
		} 
                    // update status
                    Configuration.getPlug(id).setCurState(newState);
                    System.out.println("Changed plug status (id "+id+") to "+newState+".");
                    
                    // send current state to all clients
                    response = Messages.createMessageType2(Configuration.getPlugList());
                    serverInstance.sendMessageToWorker(response);
                    break;
                case 3:
                    if(loggedIn){
                        // already logged in
                        return;
                    }
                    // check password
                    if( AccessControl.login(header.get("User"), header.get("Password")) == null){
                        // login failed
                        sendMessage(Messages.createMessageType100());
                        return;
                    }
                    // login successful, set flag
                    loggedIn = true;

                    // send back switchable plug info
                    response = Messages.createMessageType2(Configuration.getPlugList());
                    sendMessage(response);
                    break;
                case 4:
                    // logout
                    loggedIn = false;
                    // stop this thread
                    this.interrupt();
                    break;
                case 100:
                    // error occured
                    break;
                case 201:
                    // add new plug
                    Plug newPlug = new Plug(Integer.parseInt(header.get("Id")), header.get("Name"), Integer.parseInt(header.get("Code")), 0);
                    if(!Configuration.addPlug(newPlug)){
                        // error
                        sendMessage(Messages.createMessageType100());
                    }else{
                        response = Messages.createMessageType2(Configuration.getPlugList());
                        serverInstance.sendMessageToWorker(response);
                    }
                    break;
                case 202:
                    // remove plug
                    if(!Configuration.removePlug(Integer.parseInt(header.get("Id")))){
                        // error
                        sendMessage(Messages.createMessageType100());
                    }else{
                        response = Messages.createMessageType2(Configuration.getPlugList());
                        serverInstance.sendMessageToWorker(response);
                    }
                    break;
                case 203:
                    // add new user
                    if(!AccessControl.addUser(header.get("User"),header.get("Password"))){
                        // error
                        sendMessage(Messages.createMessageType100());
                    }
                    break;
                case 204:
                    // remove user
                    if(!AccessControl.removeUser(header.get("User"),header.get("Password"))){
                        // error
                        sendMessage(Messages.createMessageType100());
                    }
                    break;
                case 205:
                    // change user password
                    if(!AccessControl.changeUserPassword(header.get("User"),header.get("OldPassword"),header.get("NewPassword"))){
                        // error
                        sendMessage(Messages.createMessageType100());
                    }
                    break;
                default: 
                    // no matching type
                    sendMessage(Messages.createMessageType100());
                    break;
            }
        }
    }

    /**
     * Return login state
     * @return true iff logged in
     */
    public boolean isLoggedIn() {
        return loggedIn;
    }
}

package homeautomationserver;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Manages a single user
 * @author admin
 */
public class User {
    
    private static final int SALT_BYTE_SIZE=16;
    public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";
    
    private final String name;
    private String hash;
    
    /**
     * Copy constructor
     * @param u another user
     */
    public User(User u){
        this.name = u.name;
        this.hash = u.hash;
    }
    
    /**
     * Constructor
     * @param name username
     * @param hash password hash
     */
    public User(String name, String hash){
        this.name = name;
        this.hash = hash;
    }
    
    /**
     * Default constructor
     * @param name username
     * @param pw password
     * @param info comment
     */
    public User(String name, String pw, String info){
        this.name = name;
        byte tmpPwHash[];
        byte tmpSalt[];
        try{
            // generate new random salt for each user
            SecureRandom random = new SecureRandom();
            tmpSalt = new byte[SALT_BYTE_SIZE];
            random.nextBytes(tmpSalt);
            // hash password
            KeySpec spec = new PBEKeySpec(pw.toCharArray(),tmpSalt,65536,128);
            SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
            tmpPwHash = skf.generateSecret(spec).getEncoded();
            // store password hash with salt
            this.hash = bytesToHexString(tmpSalt)+":"+bytesToHexString(tmpPwHash);
        }catch(NoSuchAlgorithmException | InvalidKeySpecException e){
            throw new RuntimeException("Error creating password hash", e);
        }
    }
    
    /**
     * Verify password
     * @param pw password to be verified
     * @return true iff password match
     */
    public boolean checkPassword(String pw){
        try{
            //  split hash in salt and password
            String[] tmp = hash.split(":");
            byte salt[] = hexStringToBytes(tmp[0]);
            byte pwHash[] = hexStringToBytes(tmp[1]);    
            // hash parameter pw
            KeySpec spec = new PBEKeySpec(pw.toCharArray(),salt,65536,128);
            SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
            byte newHash[] = skf.generateSecret(spec).getEncoded();
            // compare created hash with stored one
            return compareHash(newHash, pwHash);
        }catch(NoSuchAlgorithmException | InvalidKeySpecException e){
            throw new RuntimeException("Error password comparison", e);
        }
    }
    
    /**
     * Change user password
     * @param pw new password
     * @return true iff successful
     */
    public boolean changePassword(String pw){
        byte tmpPwHash[];
        byte tmpSalt[];
        try{
            // generate new random salt for each password
            SecureRandom random = new SecureRandom();
            tmpSalt = new byte[SALT_BYTE_SIZE];
            random.nextBytes(tmpSalt);
            // hash password
            KeySpec spec = new PBEKeySpec(pw.toCharArray(),tmpSalt,65536,128);
            SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
            tmpPwHash = skf.generateSecret(spec).getEncoded();
            // store password hash with salt
            this.hash = bytesToHexString(tmpSalt)+":"+bytesToHexString(tmpPwHash);
            return true;
        }catch(NoSuchAlgorithmException | InvalidKeySpecException e){
            throw new RuntimeException("Error changing password hash", e);
        }
    }
    
    /**
     * 
     * @param bytes input bytes
     * @return bytes as hexadecimal string
     */
    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
            sb.append(String.format("%02x",b));
        }
        return sb.toString();
    }
    
    /**
     * 
     * @param string hexadecimal string
     * @return bytes array
     */
    private static byte[] hexStringToBytes(String string) {
        byte bytes[] = new byte[string.length() / 2];
        for(int i=0; i<bytes.length; i++){
            bytes[i]=(byte)Integer.parseInt(string.substring(2*i, 2*i+2),16);
        }            
        return bytes;
    }
    
    /**
     * Compare two hash values
     * @param a first hash
     * @param b second hash
     * @return true iff identical
     */
    private static boolean compareHash(byte[] a, byte[] b)
    {
        if(a.length != b.length){
            return false;
        }
        for(int i = 0; i < a.length && i < b.length; i++){
            if(a[i]!=b[i])return false;
        }
        return true;
    }

    /**
     * 
     * @return username
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @return password hash
     */
    public String getHash() {
        return hash;
    }
    
}

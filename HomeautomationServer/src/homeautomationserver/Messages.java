package homeautomationserver;

import java.util.List;

/**
 * Manages all messages
 * @author admin
 */
public class Messages {
    
     /**
     * Type 0 - Connection accepted
     * @return message as String
     */
    public static String createMessageType0(){
        String message = "Type: 0;";
        message += ";";
        return message;
    }
    
    /**
     * Type 1 - Switch plug
     * @param id plug id
     * @param state new state
     * @return message as String
     */
    public static String createMessageType1(int id, int state){
        String message = "Type: 1;";
        message += "Id: "+id+";";
        message += "State: "+state+";";
        message += ";";
        return message;
    }
    
    /**
     * Type 2 - Contains all plug information (also response after login)
     * @param list plug list
     * @return message as String
     */
    public static String createMessageType2(List<Plug> list){
        String message = "Type: 2;";
        message += "Length: "+ list.size() +";";
        // append all available plugs
        int counter = 1;
        for(Plug p: list){
            message += "Plug"+counter+": "+p.getId()+","+p.getName()+","+p.getCurState()+";";
            counter++;
        }
        message += ";";
        return message;
    }

    /**
     * Type 3 - Login message
     * @param username username
     * @param pw password
     * @return message as String
     */
    public static String createMessageType3(String username, String pw){
        String message = "Type: 3;";
        message += "User: "+username+";";
        message += "Password: "+pw+";";
        message += ";";
        return message;
    }
    
    /**
     * Type 4 - Logout message
     * @param username username
     * @param pw password
     * @return message as String
     */
    public static String createMessageType4(String username, String pw){
        String message = "Type: 4;";
        message += ";";
        return message;
    }
    
    /**
     * Type 100 - Error message
     * @return message as String
     */
    public static String createMessageType100(){
        String message = "Type: 100;";
        message += ";";
        return message;
    }
    
    /**
     * Type 201 - Add plug
     * @param id new plug id
     * @param name plug name
     * @param code plug code
     * @return message as String
     */
    public static String createMessageType201(int id, String name, int code){
        String message = "Type: 201;";
        message += "Id: "+id+";";
        message += "Name: "+name+";";
        message += "Code: "+code+";";
        message += ";";
        return message;
    }

    /**
     * Type 202 - Remove plug
     * @param id plug id
     * @return message as String
     */
    public static String createMessageType202(int id){
        String message = "Type: 202;";
        message += "Id: "+id+";";
        message += ";";
        return message;
    }
    
    /**
     * Type 203 - Add user
     * @param name username
     * @param pw user password
     * @return message as String
     */
    public static String createMessageType203(String name, String pw){
        String message = "Type: 203;";
        message += "User: "+name+";";
        message += "Password: "+pw+";";
        message += ";";
        return message;
    }

    /**
     * Type 204 - Remove user
     * @param name username
     * @param pw user password
     * @return message as String
     */
    public static String createMessageType204(String name, String pw){
        String message = "Type: 204;";
        message += "User: "+name+";";
        message += "Password: "+pw+";";
        message += ";";
        return message;
    }
    
    /**
     * Type 205 - Change user password
     * @param name username
     * @param oldPw old password
     * @param newPw new password
     * @return message as String
     */
    public static String createMessageType205(String name, String oldPw, String newPw){
        String message = "Type: 205;";
        message += "User: "+name+";";
        message += "OldPassword: "+oldPw+";";
        message += "NewPassword: "+newPw+";";
        message += ";";
        return message;
    }
}

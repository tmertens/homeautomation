package homeautomationserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages all plugs and corresponding plugs.txt
 * 
 * @author admin
 */
public class Configuration {

    private static List<Plug> plugList = null;
    
    /**
     * Loads initial plug configuration
     */
    public static synchronized void init(){
        plugList = loadConfiguration();
    }
    
    /**
     * return current plug List
     * @return plug list
     */
    public static synchronized List<Plug> getPlugList(){
        return plugList;
    }
    
    /**
     * Get specific plug
     * @param id plug identification
     * @return plug object
     */
    public static synchronized Plug getPlug(int id){
        for(Plug p: plugList){
            if(p.getId()==id){
                return p;
            }
        }
        return null;
    }
    
    /**
     * Adds new plug to list
     * @param newPlug new plug object
     * @return status
     */
    public static synchronized boolean addPlug(Plug newPlug){        
        // check if new ID is unique
        if(getPlug(newPlug.getId())!=null){
            return false;
        }
        // add new switchable plug to list
        plugList.add(newPlug);
        writeConfiguration();
        return true;
    }
    
    /**
     * Removes plug from list
     * @param id removed plug idenditification
     * @return status
     */
    public static synchronized boolean removePlug(int id){
        // lookup switchable plug
        Plug remove = getPlug(id);
        if(remove == null){
            return false;
        }
        // remove plug from list
        plugList.remove(remove);
        writeConfiguration();
        return true;
    }
    
    /**
     * Loads configuration file
     * @return list of plugs
     */
    private static synchronized List<Plug> loadConfiguration(){
        // store switchable plugs in list
        List<Plug> result = new ArrayList<>();
        
        try{            
            // open config
            InputStream is = Configuration.class.getResourceAsStream(Path.getFileConfig());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            String lineSplit[];
            Plug tmp;
            
            // read line by line
            while( (line = br.readLine()) != null ){
                // split line
                lineSplit = line.split(";");
                // add information to result (0=ID,1=name,2=code)
                tmp = new Plug(Integer.parseInt(lineSplit[0]),lineSplit[1],Integer.parseInt(lineSplit[2]),0);
                result.add(tmp);
            }
        }catch(IOException | NumberFormatException e){
            throw new RuntimeException("Error loading configuration file", e);
        }
        
        return result;
    }
    
    /**
     * Writes configuration file
     */
    private static synchronized void writeConfiguration(){
        try(PrintWriter writer = new PrintWriter(Path.getPathConfig(), "UTF-8")) {
            // write all plugs to file
            for(Plug p: plugList){
                writer.println(p.getId()+";"+p.getName()+";"+p.getCode());
            }
        }catch(Exception e){
            throw new RuntimeException("Error writing configuration file", e);
        }
    }
    
}

package homeautomationserver;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManagerFactory;

/**
 * A multithreaded server instance
 * @author admin
 */
public class MultiThreadedServer extends Thread{

    private final int serverPort;
    private SSLServerSocket serverSocket = null;
    private List<Worker> workerList = null;
    
    /**
     * Default constructor
     * @param port server port
     */
    public MultiThreadedServer(int port){
        this.serverPort=port;
        workerList = new ArrayList<>();
    }
    
    @Override
    public void run() {
        openServerSocket();
        while(!this.isInterrupted()){
            SSLSocket clientSocket = null;
            try{
                clientSocket = (SSLSocket) this.serverSocket.accept();
            }catch(IOException e){
                if(this.isInterrupted()){
                    // Server stopped
                    return;
                }
                throw new RuntimeException("Error accepting client connection", e);
            }
            // create new Thread
            Worker newWorker = new Worker(this,clientSocket);
            addWorker(newWorker);
            new Thread(newWorker).start();
            System.out.println("New client connection accepted.");
        }
        
    }
    
    /**
     * Opens SSL server socket on specified port
     */
    private void openServerSocket(){
        try {
            System.setProperty("javax.net.ssl.keyStore", Path.getPathJKS());
            System.setProperty("javax.net.ssl.keyStorePassword", "server");
            System.setProperty("javax.net.ssl.keyStoreType", "JKS");
            
            // open TLS connection
            KeyStore store = KeyStore.getInstance("JKS");
            store.load(new FileInputStream(Path.getPathJKS()), "server".toCharArray());
            
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(store);
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(store, "server".toCharArray());
            // init SSL context
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(kmf.getKeyManagers(), tmf.getTrustManagers(),new java.security.SecureRandom());
            // open client socket
            SSLServerSocketFactory factory = context.getServerSocketFactory();
            serverSocket = (SSLServerSocket)factory.createServerSocket(this.serverPort);
                
            //this.serverSocket = new ServerSocket(this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Error opening port", e);
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException | KeyManagementException e) {
            throw new RuntimeException("Error opening SSL socket", e);
        }
    }

    /**
     * Remove worker from list
     * @param w removed worker
     */
    public synchronized void removeWorker(Worker w) {
        workerList.remove(w);
    }
    
    /**
     * Add worker from lsit
     * @param w new worker
     */
    private synchronized void addWorker(Worker w) {
        workerList.add(w);
    }
    
    /**
     * Broadcast message to all workers in list
     * @param message message to be send
     */
    public synchronized void sendMessageToWorker(String message){
        for(Worker w: workerList){
            if(w.isLoggedIn()){
                w.sendMessage(message);
            }
        }
    }
}

package homeautomationserver;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages all users and corresponding users.txt
 * 
 * @author admin
 */
public class AccessControl {
    
    private static List<User> users = null;
    
    /**
     * Loads users from file
     */
    public static synchronized void init(){
        users = loadUsers();
    }
    
    /**
     * Verifies user password
     * @param username username
     * @param pw password
     * @return logged in user object (otherwise null)
     */
    public static synchronized User login(String username, String pw){
        // lookup user
        for(User u: users){
            if(u.getName().equals(username)){
                // check password
                if(u.checkPassword(pw)){
                    return u;
                }
            }
        }
        // otherwise return null
        return null;
    }
    
    /**
     * Changes user password
     * @param username username
     * @param oldPw old password
     * @param newPw new password
     * @return status of change
     */
    public static synchronized boolean changeUserPassword(String username, String oldPw, String newPw){
        // lookup user
        for(User u: users){
            if(u.getName().equals(username)){
                // change password if old password valid
                if(u.checkPassword(oldPw) && u.changePassword(newPw)){
                    writeFile();
                    return true;
                }
            }
        }
        // otherwise return false
        return false;
    }
    
    /**
     * Loads user file
     * @return 
     */
    private static synchronized List<User> loadUsers(){
        // store users in list
        List<User> result = new ArrayList<>();
        try{            
            // open config
            InputStream is = AccessControl.class.getResourceAsStream(Path.getFileUser());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            String lineSplit[];
            User tmp;
            // read line by line
            while( (line = br.readLine()) != null ){
                // split line
                lineSplit = line.split(";");
                // add information to result (0=name,1=hash)
                tmp = new User(lineSplit[0],lineSplit[1]);
                // add user to list
                result.add(tmp);
            }
        }catch(Exception e){
            throw new RuntimeException("Error loading user list", e);
        }
        return result;
    }
    
    /**
     * Writes user file
     */
    private static synchronized void writeFile(){
        try(PrintWriter writer = new PrintWriter(Path.getPathUser(), "UTF-8")) {
            // write all users to file
            for(User u: users){
                writer.println(u.getName()+";"+u.getHash());
            }
        }catch(Exception e){
            throw new RuntimeException("Error closing server", e);
        }
    }
    
    /**
     * Adds new user
     * @param name new username
     * @param pw new password
     * @return status
     */
    public static synchronized boolean addUser(String name, String pw){
        // check for user with same name (name has to be unique)
        for(User u: users){
            if(u.getName().equals(name)){
                return false;
            }
        }
        users.add(new User(name, pw, ""));
        // write to file
        writeFile();
        return true;
    }
    
    /**
     * Removes new user
     * @param name username
     * @param pw password
     * @return status
     */
    public static synchronized boolean removeUser(String name, String pw){
        // check for user with same name (name has to be unique)
        for(User u: users){
            if(u.getName().equals(name) && u.checkPassword(pw)){
                users.remove(u);
                writeFile();
                return true;
            }
        }
        return false;
    }
   
}

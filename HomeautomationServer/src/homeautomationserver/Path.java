package homeautomationserver;

/**
 * Class managing paths to config.txt and user.txt
 * @author admin
 */
public class Path {
    
    private static String fileUser;
    private static String fileConfig;
    private static String fileJKS;
    private static String pathUser;
    private static String pathConfig;
    private static String pathJKS;
    
    /**
     * Default constructor initliasing all paths
     * @param fileU user filename
     * @param fileC configuration filename
     */
    public Path(String fileU, String fileC){
        try{
            fileUser = fileU;
            fileConfig = fileC;
            fileJKS = "server.jks";
            // Get required files in current directory of class files
            pathUser = getClass().getResource(fileUser).toURI().getPath();
            pathConfig = getClass().getResource(fileConfig).toURI().getPath();
            pathJKS = getClass().getResource(fileJKS).toURI().getPath();
        }catch(Exception e){
            throw new RuntimeException("Error init paths", e);
        }    
    }

    /**
     * 
     * @return user paths
     */
    public static String getPathUser() {
        return pathUser;
    }

    /**
     * 
     * @return config path
     */
    public static String getPathConfig() {
        return pathConfig;
    } 
    
    /**
     * 
     * @return JKS path
     */
    public static String getPathJKS() {
        return pathJKS;
    } 

    /**
     * 
     * @return user filename
     */
    public static String getFileUser() {
        return fileUser;
    }

    /**
     * 
     * @return config filename
     */
    public static String getFileConfig() {
        return fileConfig;
    }
    
}

package homeautomationserver;

/**
 * Manages a single plug
 * @author admin
 */
public class Plug {
    
    private final int id;
    private final String name;
    private final int code;
    private int curState;
    
    /**
     * Default constructor
     * @param id plug id
     * @param name plug name
     * @param code plug code
     * @param curState initial state
     */
    public Plug(int id, String name, int code, int curState){
        this.id = id;
        this.name = name;
        this.code = code;
        this.curState = curState;
    }
   
    /**
     * 
     * @return plug id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @return plug name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @return plug code
     */
    public int getCode() {
        return code;
    }
    
    /**
     * 
     * @param curState new state
     */
    public synchronized void setCurState(int curState) {
        this.curState = curState;
    }
    
    /**
     * 
     * @return current state
     */
    public int getCurState() {
        return curState;
    }
    
}

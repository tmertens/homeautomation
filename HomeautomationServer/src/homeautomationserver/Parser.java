package homeautomationserver;

import java.util.HashMap;

/**
 * Parses incoming messages
 * @author admin
 */
public class Parser {
    
    /**
     * @param plainHeader Received header as String
     * @return HashMap of header content
     */
    public static HashMap<String, String> parseHeader(String plainHeader){
        HashMap<String, String> header = new HashMap<>();
        String[] parts = plainHeader.split(";");
        String key = "";
        String value = "";
        for (String part : parts) {
            int first_occ = part.indexOf(":");
            if (first_occ>0) {
                key = part.substring(0, first_occ);
                value = part.substring(first_occ + 2, part.length());
            }
            header.put(key,value);
        }
        return header;
    }
    
}

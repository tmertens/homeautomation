package homeautomationserver;

/**
 * Server main class
 * @author admin
 */
public class Server {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // init paths
        Path path = new Path("users.txt","config.txt");
        // lnit access control
        AccessControl.init();
        // init configuration
        Configuration.init();
               
        // start server
        MultiThreadedServer server = new MultiThreadedServer(12345);
        server.start();        
    }
}
